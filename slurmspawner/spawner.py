from batchspawner import SlurmSpawner
import shlex
import subprocess
from datetime import datetime
from jinja2 import Environment, PackageLoader, select_autoescape
from traitlets import default

class FormSlurmSpawner(SlurmSpawner):

    def _render_form(self, current_spawner):

        username = current_spawner.user.name
        if '@' in username:
            username = username[:username.find('@')]

        # partitions, default partitions and gres
        default_partition = None
        gres = {}
        sinfo = subprocess.run(shlex.split('sinfo -h -t "idle,allocated,mixed" -O "Partition,Gres:150,GresUsed:150"'), capture_output=True, text=True)
        for sinfo_line in sinfo.stdout.splitlines():
            partition, all_gres, used_gres = sinfo_line.split()
            if partition.endswith('*'):
                partition = partition[:-1]
                default_partition = partition
            gres_list = []
            all_gres_items = all_gres.split(',')
            used_gres_items = used_gres.split(',')
            for all_gres_item, used_gres_item in zip(all_gres_items, used_gres_items):
                if all_gres_item != '(null)':
                    all_gres_item, used_gres_item = [i.split('(')[0] for i in (all_gres_item, used_gres_item)]
                    gres_type, gres_name, gres_qty = all_gres_item.split(':')
                    gres_used = used_gres_item.split(':')[2]
                    if gres_type == 'gpu' and gres_used < gres_qty:
                            gres_list.append(gres_name)
            if partition not in gres:
                gres[partition] = set()
            gres[partition].update(gres_list)


        # account and partitions
        accounts = {}
        sacctmgr = subprocess.run(shlex.split(f'sacctmgr list -n -P associations user={username} format=account%250,partition%250'), capture_output=True, text=True)
        for sacctmgr_line in sacctmgr.stdout.splitlines():
            account, partition = sacctmgr_line.split('|')
            if account not in accounts:
                accounts[account] = set()

            if partition == '':
                accounts[account].update(gres.keys())
            else:
                accounts[account].add(partition)

        # reservations
        reservations = {}
        scontrol = subprocess.run(shlex.split('scontrol show reservation -o'), capture_output=True, text=True)
        scontrol_result = scontrol.stdout
        if "No reservations in the system" not in scontrol_result:
            for reservation_line in scontrol_result.splitlines():
                reservation = {}
                is_usable = False
                for reservation_part in reservation_line.split(' '):
                    parts = reservation_part.split('=')
                    if len(parts) == 2 and parts[1] != '(null)':
                        reservation[parts[0]] = parts[1]
                    else:
                        reservation[parts[0]] = None

                reservation_start = datetime.strptime(reservation['StartTime'], "%Y-%m-%dT%H:%M:%S")
                if datetime.now() < reservation_start:
                    next

                if reservation['Accounts'] is not None:
                    reservation['Accounts'] = reservation['Accounts'].split(',')
                    reservation['Accounts'] = list(set(accounts.keys()) & set(reservation['Accounts']))
                    is_usable = len(reservation['Accounts']) > 0
                else:
                    reservation['Accounts'] = []

                if reservation['PartitionName'] is not None:
                    reservation_accounts_count = len(reservation['Accounts'])
                    for account in reservation['Accounts']:
                        if reservation['PartitionName'] not in accounts[account]:
                            del reservation['Accounts'][account]

                    if len(reservation['Accounts']) == 0 and reservation_accounts_count > 0:
                        is_usable = False


                if reservation['Users'] is not None:
                    reservation['Users'] = reservation['Users'].split(',')
                    is_usable = is_usable or username in reservation['Users']


                if is_usable:
                    reservations[reservation['ReservationName']] = {
                        'partition': reservation['PartitionName'],
                        'accounts': reservation['Accounts']
                    }


        # default account
        sacctmgr = subprocess.run(shlex.split(f'sacctmgr -n -P list user {username} format=DefaultAccount'), capture_output=True, text=True)
        default_account = sacctmgr.stdout[:-1]

        env = Environment(loader=PackageLoader("slurmspawner"), autoescape=select_autoescape())
        template = env.get_template('slurm_form.html')
        return template.render(accounts=accounts, gres=gres, reservations=reservations, default_account=default_account, default_partition=default_partition)

    @default('options_form')
    def _options_form_default(self):
        return self._render_form

    def options_from_form(self, formdata):
        options = {}
        options['account'] = formdata['account'][0]
        options['partition'] = formdata['partition'][0]
        options['nprocs'] = formdata['cpu'][0]
        options['memory'] = f"{formdata['memory'][0]}gb"
        options['runtime'] = f"{formdata['runtime'][0]}:00:00"
        if 'gres' in formdata and formdata['gres'][0] != '':
            options['gres'] = f"gpu:{formdata['gres'][0]}:{formdata['gres_qty'][0]}"
        if formdata['reservation'][0] != '':
            options['options'] = f"--reservation={formdata['reservation'][0]}"
        return options
