import sys

from setuptools import find_packages, setup

setup(
    name='slurmspawner',
    version='0.1.0',
    install_requires=[
        'batchspawner @ git+https://github.com/jupyterhub/batchspawner.git',
        'jinja2',
        'jupyterhub>=1.4.2'
    ],
    python_requires='>=3.7',
    description='JupyterHub Spawner for SLURM with options form',
    url='https://gitlab.com/ifb-elixirfr/cluster/utils/slurmspawner',
    author='Julien Seiler',
    author_email='seilerj@igbmc.fr',
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    license='BSD',
    packages=find_packages(),
    include_package_data=True,
    project_urls={
        'Source': 'https://gitlab.com/ifb-elixirfr/cluster/utils/slurmspawner',
        'Bug reports': 'https://gitlab.com/ifb-elixirfr/cluster/utils/slurmspawner/-/issues',
    },
)
